
圈子接口
==============

**服务部署目录：**   ```121.42.137.3:/home/birdy/```

**接口请求地址：**   ```http://121.42.137.3:8092/api/helloworld```

文档更新版本说明

---------
##  ==============================
###  *2017-10-08 问题及新需求*
**1、注册完成后，用户信息页没有显示用户的昵称（显示的是：请修改名字）**  
**2、新开发接口没有添加token**  
**3、打赏完成后，打赏人数没有自动加1**  
**4、增加圈子用户信息**  
	第一步：先加只显示角色信息  
	第二部：医生、专家、供应商登陆时检查信息是否完善。医生和专家填写医院和科室信息；供应商填写领域和品牌信息。并在圈子中显示（替换现在的角色显示）。其他能发送主题的其他用户在圈子只显示角色信息。  
**5、隐藏自己发送主题的关注按钮（自己不能关注自己）**  
**6、取消关注，要删除联系人**  
##  ==============================


---------
##  ==============================
###  *2017-09-16*
*  增加接口：修改用户昵称

######截止今日问题列表
**1、注册页面提交没反应【开发完要测试】**  
**2、注册页面邀请码输入框是密码类型的**  
**3、个人界面的昵称显示的不是注册时填写的昵称**  
【今天给api增加了更新昵称接口跟这个一块处理吧】  
**4、发圈子失败【请检查昨天更新的第一条是不是已做了修改】**  
**5、更新头像的后个人界面头像并未更新**  
**6、圈子没有打赏人数


##  ==============================
##  ==============================
###  *2017-09-15*
*  ```1、发布主题1 —— 纯文字、外链接```及```2、发布主题2 —— 视频、音频、图片```中参数 ```docid```变更为```user_id```
*  ```5、获取圈子信息```和```14、获取主题```两个接口返回值中```doctor```改为```creator```
*  邀请码获取网页

##  ==============================
##  ==============================
###  *2017-09-10*
*  8种角色 ```1:超级管理员；2:管理员；3:客服；4:专家；5:医生；6:供应商；7:VIP用户；8:普通用户；```
*  获取邀请码接口
*  更新注册接口
*  登陆成功带角色(role)数据

##  ==============================
##  ==============================
###  *2017-08-19*
*  imId统一变更为加密后的字母字符串
*  IM服务器就绪后，需要清理了IM的用户数据，然后app端退出重新登录方可生成IM用户ID

##  ==============================
##  ==============================
###  *2017-08-17*
*  登录接口返回数据增加imId
*  圈子数据增加了是否关注[isFollow]、打赏人数[donateNum],topic>doctor增加imId
*  增加webhook接口
*  增加问诊创建订单接口
*  增加问诊发送消息接口
*  增加同步IM的id的接口(IM服务调用)
*  打赏生成订单接口参数名amount改为price

##  ==============================


1、发布主题1 —— 纯文字、外链接
---------

**[POST]**		```/api/qz/topic/addcontent```

**PARAMS：**
```
			user_id		//发布者id
			content		//文本
			url			//外链接
```

**RESPONSE：**
```
			{
			  "result": {},
			  "success": true
			}
```

2、发布主题2 —— 视频、音频、图片
---------

**[POST]**			```/api/qz/topic/addmedia```

**[Content-Type]**	```multipart/form-data;```

**PARAMS：**
```
			user_id		//发布者id
			content		//文本
			media		//图片（多文件使用同一参数名）、视频、音频
			type		[V：视频；A：音频；I：图文；]
```

**RESPONSE：**

图片：
```
	{
			"result": {
			"media": "http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/39.jpg,http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/40.jpg"
			},
			"success": true
		}
```

音频
```
	  {
		  "result": {
		    "media": "http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/media/32.wav"
		  },
		  "success": true
		}
```

3、图片资源说明
---------

**图片有三种尺寸：**
```
		qz120		//缩略图
		qz960		//大图
		qz1400		//原图
```

**请求例子：**
```
		http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/0.jpg@!qz120
		http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/0.jpg@!qz960
		http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/0.jpg@!qz1400
```

4、删除主题
---------
*当此用户id等于主题创建者的id时，可以删除此条评论*

**[POST]**		```/api/qz/topic/del```

**PARAMS：**
```
				topic_id
```

**RESPONSE：**

```
				{
				  "result": {},
				  "success": true
				}
```



5、获取圈子信息
---------
*主题带3条评论*

**[GET]**			```/api/qz/topics```

**PARAMS：**
```
				user_id		//用户ID
```

**RESPONSE：**
```
				{
				    "result": [
				        {
				            "id": 107,
				            "donateNum": 0,				//打赏人数
				            "isFollow": 1,				//是否已关注
				            "sumFollow": 1,				//关注人数
				            "createTime": "2017-07-24 23:24:53",
				            "creator": {
				                "id": 1,,
				                "name": "李医生(工号001)"
				                "username": "13379031685",
				                "imId": null,
				                "role": {
				                    "id": 5,
				                    "name": "DOCTOR",
				                    "descr": "医生",
				                    "level": 12
				                }
				            },
				            "type": "I",
				            "sumComments": 1,
				            "sumLike": 0,
				            "content": "小图",
				            "qzImgs": [
				                {
				                    "url": "http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/132.jpg"
				                },
				                {
				                    "url": "http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/133.jpg"
				                },
				                {
				                    "url": "http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/qz/img/134.jpg"
				                }
				            ],
				            "like": 0,
				            "atnti": 0,
				            "comments": [
				                {
				                    "id": 53,
				                    "parentCommentId": 0,
				                    "createTime": "2017-07-31 00:34:42",
				                    "creator": 471,
				                    "creatorName": "刘小晓",
				                    "creatorType": 1,
				                    "topicId": 107,
				                    "sumLike": 0,
				                    "like": 0,
				                    "text": "还在家呢"
				                }

				                ...

				            ]
				        }

				        ...

				    ],
				    "total": 64,
				    "success": true
				}
```


6、发表评论
---------

**[POST]**		```/api/qz/comment/add```

**PARAMS：**
```
				user_id			//发布者id（医生或病人）
				topic_id		//主题id
				comment_id		//如果是回复别人的留言，则需要此留言的id
				content			//评论内容
```

**RESPONSE：**
```
				{
				  "result": {},
				  "success": true
				}
```


7、删除评论
---------
*当此用户id等于评论创建者的id时，可以删除此条评论*

**[POST]**		```/api/qz/comment/del```

**PARAMS：**
```
				comment_id		//评论ID
```

**RESPONSE：**
```
				{
				  "result": {},
				  "success": true
				}
```

8、获取评论列表（30条）
---------

**[GET]**		```/api/qz/comments```

**PARAMS：**
```
				user_id
				topic_id		//主题id
```

**RESPONSE：**
```
				{
				    "result": [
				        {
				            "id": 28,
				            "parentCommentId": 0,,
				            "ParentCommentCreator": "李雷"		//回复李雷
				            "createTime": "2017-07-12 15:12:22",
				            "creator": 1,
				            "creatorType": 1,
				            "topicId": 76,
				            "sumLike": 0,
				            "like": 0,
				            "text": "测试评论2"
				        },
				        {
				            "id": 27,
				            "parentCommentId": 0,
				            "createTime": "2017-07-12 15:12:22",
				            "creator": 1,
				            "creatorType": 1,
				            "topicId": 76,
				            "sumLike": 0,
				            "like": 0,
				            "text": "测试评论2"
				        }

				        ...

				    ],
				    "total": 0,
				    "success": true
				}
```


9、点赞取消点赞 —— 主题、评论
---------

**[POST]**		```/api/qz/topic/like```

**PARAMS：**
```
				user_id			// 操作者id（医生或病人）
				obj_id			// 主题id | 评论id
				type			// 点赞的对象 T:主题；C:评论；
```

**RESPONSE：**
```
				{
				    "result": {
				        "id": 113,
				        "sumComments": 0,
				        "sumFollow": 0,
				        "sumLike": 2,		//点赞数
				        "like": 0,			//点赞标识 0：未赞；1：已赞；
				        "atnti": 0
				    },
				    "success": true
				}
```

10、获取新增主题（20条）
---------

**[GET]**		```/api/qz/topic/refresh```

**PARAMS：**
```
				user_id
				topic_id
```


11、获取旧主题（10条）
---------

**[GET]**		```/api/qz/topic/more```

**PARAMS：**
```
				user_id
				topic_id
```



12、获取新增评论（30条）
---------

**[GET]**		```/api/qz/comment/refresh```

**PARAMS：**
```
				user_id
				topic_id
				comment_id		//最前边的一条评论id
```

13、获取更多评论（15条）
---------

**[GET]**		```/api/qz/comment/more```

**PARAMS：**
```
				user_id
				topic_id
				comment_id		//最后边的一条评论id
```

14、获取主题（带30条评论）
------------

**[GET]**		```/api/qz/topic```

**PARAMS：**
```
				user_id
				topic_id
```

15、获取热门评论（3条）
-------------
**[GET]**		```/api/qz/comment/top```

**PARAMS：**
```
				user_id
				topic_id
```

16、登录成功返回数据
-------------

**RESPONSE：**
```
				{
				    "result": {
				        "id": 106,
				        "username": “13888888888”,
				        "token": “hfjskaflfjkahljfah”,
				        "name": "老李",
				        "headUrl": "http://fj",			//头像图片地址
				        "userType": 6,					//1:医生；6:普通用户
				        "addr": "西安市高新区",
        				"im_id": "4TFfsffds879s",		//im用户的ID
				        "phone": "13888888888",
				        "role": {
				            "id": 4,		//1:超级管理员；2:管理员；3:客服；4:专家；5:医生；6:供应商；7:VIP用户；8:普通用户；
				            "name": "EXPERT",
				            "descr": "专家",
				            "level": 11
				        }
				    },
				    "success": true
				}
```

17、头像上传
-----------
**[POST]** 			```/api/normal_user/headimg```

**[Content-Type]**	```multipart/form-data;```

**PARAMS：**
```
				user_id
				image		//图片文件
```

**RESPONSE：**
```
				{
				    "result": {
				        "headUrl": "http://aierlife.oss-cn-qingdao.aliyuncs.com/aier/doc/headimg/1.png"
				    },
				    "success": true
				}
```


*支付流程说明（已打赏为例）
---------
先请求生成订单接口		```/api/donate/create```
通过返回的order_id获取charge		```/api/pingpp/charge_appointment```



18、打赏生成订单
---------

**[POST]**		```/api/donate/create```

**PARAMS：**
```
				user_id
				topic_id
				price		//金额[元]
```

**RESPONSE：**
```
				{
				    "result": {
				        "id": 7700,			//order_id
				        "serialNumber": "201708082106126121502197572121",
				        "payState": 1,
				        "price": 10,
				        "createTime": "2017-08-08 21:06:12",
				        "validState": 1,
				        "payerId": 10,
				        "payerName": "卫超鹏",
				        "payeeId": 1,
				        "payeeName": "李医生(工号001)"
				    },
				    "success": true
				}
```

19、支付获取charge
---------

**[POST]**		```/api/pingpp/charge_appointment```

**PARAMS：**
```
				order_id
				pay_type		//1：微信；2：支付宝
				order_type		//donate:打赏;inquiry:问诊
```

**RESPONSE：**
```
				{
				    "result": {
				        "order_no": "2017080820573057301502197050577",
				        "time_expire": 1502204366,
				        "metadata": {
				            "charge_type": "appointment",
				            "order_type": "donate"
				        },
				        "livemode": false,
				        "subject": "2017-08-08 20:57 圈子打赏 李医生(工号001)",
				        "channel": "wx",
				        "description": null,
				        "body": "2017-08-08 20:57 圈子打赏 李医生(工号001)",
				        "failure_msg": null,
				        "refunds": {
				            "object": "list",
				            "url": "/v1/charges/ch_GmT8C4Sa9iP4nLG4eHGi1Oy9/refunds",
				            "has_more": false,
				            "data": []
				        },
				        "amount_refunded": 0,
				        "time_settle": null,
				        "time_paid": null,
				        "credential": {
				            "object": "credential",
				            "wx": {
				                "appId": "wxo0gq1k5izx18p0ef",
				                "partnerId": "1263284201",
				                "prepayId": "11010000001708080ckyhglqpo40pcad",
				                "nonceStr": "7c343bf0248129191c563e54be9a6b13",
				                "timeStamp": "1502197166",
				                "packageValue": "Sign=WXPay",
				                "sign": "166dbab7fdde5219f25bfa79ac3601e9922d1639"
				            }
				        },
				        "extra": {},
				        "refunded": false,
				        "client_ip": "127.0.0.1",
				        "currency": "cny",
				        "id": "ch_GmT8C4Sa9iP4nLG4eHGi1Oy9",
				        "app": "app_azbD4Kqz9mrHWL0m",
				        "amount": 1000,
				        "failure_code": null,
				        "created": 1502197166,
				        "amount_settle": 1000,
				        "paid": false,
				        "transaction_no": null,
				        "object": "charge"
				    },
				    "success": true
				}
```

20、问诊创建订单
---------

**[POST]**		```/api/inquiry/create```

**PARAMS：**
```
				pat_id		//病人id
				doc_id		//医生id
```

**RESPONSE：**
```
				{
				    "result": {
				        "id": 7750,
				        "serialNumber": "2017081701272527251502904445714",
				        "payState": 1,
				        "price": 30,
				        "createTime": "2017-08-17 01:27:25",
				        "validState": 1,
				        "docId": 1,
				        "docName": "李医生(工号001)",
				        "patientId": 10,
				        "patientName": "卫超鹏"
				    },
				    "success": true
				}
```

*21、关注  [关注后台没有调用IM接口]
---------

**[POST]**		```/api/qz/follow```

**PARAMS：**
```
				user_id
				topic_id
```

**RESPONSE：**
```
				{
				    "result": "cancel",		//cancel:取消关注；follow：关注
				    "success": true
				}
```


22、webhook接口（app端不用调）
---------

**[POST]**		```/api/pingpp/charge_successed```


23、问诊发送消息接口
---------

**[POST]**		```/api/inquiry/sendMsg```

**PARAMS：**
```
				user_id			//发送方api中的userId
				obj_id			//接收方IM用户ID
```

**RESPONSE：**
```
				{
				    "result": {
				        "docId": 1,
				        "patientId": 10,
				        "surplus": 2,		//还可以发送的条数，-1:医生
				        "total": 30,
				        "id": 1,
				        "canSend": 1		//能否发送
				    },
				    "success": true
				}
```


24、同步IM用户ID到api服务(IM创建完用户后调用服务调用)
---------
**特别说明：用户首次登陆完成后，IM穿件完用户后除了调用此接口，还需要给登陆返回的消息中的```im_id```赋值**

**[POST]**		```/api/normal_user/synimid```

**PARAMS：**
```
				user_id			//api用户ID
				im_id			//IM用户ID
```

**RESPONSE：**
```
				{
				    "result": null,
				    "success": true
				}
```



25、获取邀请码
**[GET]**		```http://localhost:9082/api/normal_user/invitation_code/{role_id}```

**PARAMS：**
```
				role_id		//1:超级管理员；2:管理员；3:客服；4:专家；5:医生；6:供应商；7:VIP用户；8:普通用户；
```

**RESPONSE：**
```
				{
				    "result": {
				        "code": "gqfxzb",
				        "createTime": "2017-09-12 00:08:47",
				        "owner": null,
				        "role": {
				            "id": 4,
				            "name": "EXPERT",
				            "descr": "专家",
				            "level": 11
				        }
				    },
				    "success": true
				}
```

26、注册接口

**[POST]**		```/api/normal_user/register```

**PARAMS：**
```
				username
				password
				sms_code			//验证码
				invitation_code		//邀请码
				name				//昵称
```

**RESPONSE：**
```
			{
			    "result": {
			        "id": 966,
			        "username": "13022800037",
			        "createTime": "2017-09-12 02:41:09",
			        "disable": null,
			        "inviter": null,
			        "userRole": null,
			        "imId": null,
			        "role": {
			            "id": 4,	//1:超级管理员；2:管理员；3:客服；4:专家；5:医生；6:供应商；7:VIP用户；8:普通用户；
			            "name": "EXPERT",
			            "descr": "专家",
			            "level": 11
			        },
			        "name": null,
			        "gender": null,
			        "identityCard": null,
			        "address": null,
			        "patients": [],
			        "utype": 0,
			        "headUrl": null,
			        "phone": null
			    },
			    "success": true
			}
```



27、修改用户昵称
---------

**[POST]**		```/api/normal_user/updtname```

**PARAMS：**
```
				user_id
				name
```

**RESPONSE：**
```
				{
				    "result": null,
				    "success": true
				}
```

28、修改用户角色
----------
**用户角色表**  
<table>
	<thead>
		<th>role_id</th>
		<th>角色名称</th>
	</thead>
	<tbody>
		<tr><td>1</td><td>超级管理员</td></tr>
		<tr><td>2</td><td>管理员</td></tr>
		<tr><td>3</td><td>客服</td></tr>
		<tr><td>4</td><td>专家</td></tr>
		<tr><td>5</td><td>医生</td></tr>
		<tr><td>6</td><td>供应商</td></tr>
		<tr><td>7</td><td>VIP用户</td></tr>
		<tr><td>8</td><td>普通用户</td></tr>
	</tbody>
</table>

**修改方法**  
```update user u set u.role_id=5 where u.username='13379031685';```  
修改里面的```role_id```和```username```参数，```role_id```参考上表;  







